# Vetsource Alias

Contains scripts to be added to bashrc file in order to create aliases that can be used for easy access

## Overview
The main idea of this repository is to add different aliases that we make use of on our daily basis to avoid having to remember different commands to execute to get something done.

## Requirements

The startvpn alias requires an additional program and environment variable configuration:
- install `expect`:
```
sudo apt install expect
```
- Add `VS_VPN_USER` environment variable with your VPN user (change `myuser` to your own):
```
echo "export VS_VPN_USER=myuser" >> ~/.bashrc
```

## Quickstart
If you want to use these commands you will have to:
1- Copy the commands you want to import
2- Edit your bashrc file:
```
vim ~/.bashrc
```
3- Go to the bottom of your bashrc file and paste the commands copied in step 1.
4- Load the bashrc file:
```
source ~/.bashrc
```

Another alternative is just to add all the file to your bashrc, spececially if this is your first time:
1- Execute:
```
cat aliases.sh >> ~/.bashrc
```
2- Load the bashrc file:
```
source ~/.bashrc
```

## Repo installation
A good way to keep up-to-date with new aliases is to git clone this repo. For example:
```
cd ~/src
git clone git@gitlab.com:mmunoz5/vetsource-alias.git
echo "source ~/src/vetsource-alias/aliases.sh" >> ~/.bashrc
source ~/.bashrc
```

## Functions
This section contains information about each one of the commands that is part of this repository.

- **checkvpn** : Checks by doing multiple pings to different IPs if the vpn is working or is down.
- **startvpn** : Stops an starts the VS VPN invoking the `/etc/openvpn/vetsource_vpn.sh` script in a non interactive way (see requirements).
- **stopvpn** : Stops the VS VPN invoking the `/etc/openvpn/vetsource_vpn.sh` script in a non interactive way.
- **clearmemcache** : Clears the memcache docker container.
- **mysqlflush** : Executes `mysqladmin flush-hosts` in the mysql docker container.
- **devstack_start** : Starts your local devstack.
- **devstack_stop** : Stops the local devstack.
- **devstack_status** : Asks for devstack status.

Compiling
- **buildtesthelper** : Builds TestHelperFx Project. It will give you further instructions if you don't have the right repo and/or path

