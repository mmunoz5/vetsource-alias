function _checkvpn {
    RED='\033[0;31m'
    GREEN='\033[0;32m'
    NC='\033[0m' # No Color

    echo "Build vetsource vpn result: "
    ping -c 2 10.138.1.11 | awk -F'/' 'END{ printf (/^rtt/? "'${GREEN}' OK '${NC}' "$5" ms":"'${RED}'FAIL'${NC}'") }'

    echo
    echo "Build vetsource vpn result: "
    ping -c 2 10.138.0.12 | awk -F'/' 'END{ printf (/^rtt/? "'${GREEN}' OK '${NC}' "$5" ms":"'${RED}'FAIL'${NC}'") }'

    echo
    echo "Node2 ping result: "
    ping -c 2 10.10.1.242 | awk -F'/' 'END{ printf (/^rtt/? "'${GREEN}' OK '${NC}' "$5" ms":"'${RED}'FAIL'${NC}'") }'

    echo
}

function clearmemcache {
    echo "Clearing memchached information"
    docker exec -it memcached bash -c "echo flush_all > /dev/tcp/localhost/11211"
    test $? -eq 0 && echo "Memcache clear success" || echo "An error was thrown while clearing up memcache. Exit code $?";
    echo "\n"
}

function mysqlflush {
    docker exec -i mysql mysqladmin flush-hosts
}

function devstack_start {
    cd ~/docker
    ./start_devstack
    cd -
}

function devstack_status {
    cd ~/docker
    ./docker_status.sh
    cd -
}

function devstack_stop {
    cd ~/docker
    ./stop_devstack
    cd -
}

function startvpn {
    OPENVPN_DIR=/etc/openvpn
    LAST_DIR=$(pwd)

    # validating requirements to run
    if [[ -z "${VS_VPN_USER}" ]]; then
        echo "Please set VS_VPN_USER env variable to your VPN username, aborting."
        return 1;
    fi
    which expect >/dev/null
    if [ $? -eq 1 ]; then
        echo "Please install required 'expect' command (sudo apt-get install expect), aborting."
        return 1;
    fi

    # stoping and starting vpn
    echo "Stoping VPN first..."
    cd ${OPENVPN_DIR} && ./vetsource_vpn.sh ${VS_VPN_USER} stop both

    echo "Starting VPN..."
    expect_commands='
    spawn ./vetsource_vpn.sh $env(VS_VPN_USER) start both
    expect "password for"
    stty -echo; interact -u tty_spawn_id -o "\r" return; stty echo;
    expect "Enter Auth Username:"
    send "$env(VS_VPN_USER)\r"
    expect "Enter Auth Password: (press TAB for no echo)"
    send "push\r"
    expect "Enter Auth Username:"
    send "$env(VS_VPN_USER)\r"
    expect "Enter Auth Password: (press TAB for no echo)"
    send "push\r"
    interact'
    cd ${OPENVPN_DIR} && expect -c "${expect_commands//
    /;}"
    cd ${LAST_DIR}
}

function stopvpn {
    OPENVPN_DIR=/etc/openvpn
    LAST_DIR=$(pwd)

    # validating requirements to run
    if [[ -z "${VS_VPN_USER}" ]]; then
        echo "Please set VS_VPN_USER env variable to your VPN username, aborting."
        return 1;
    fi

    # stoping VPN
    cd ${OPENVPN_DIR} && ./vetsource_vpn.sh ${VS_VPN_USER} stop both

    cd ${LAST_DIR}
}

function _vpn_ping {
    RED='\033[0;31m'
    GREEN='\033[0;32m'
    NC='\033[0m' # No Color
    ping -c 2 $1 | awk -F'/' 'END{ printf (/^rtt/? "'${GREEN}' OK '${NC}' "$5" ms":"'${RED}'FAIL'${NC}'") }'
    echo
}

function checkvpn {
    IP_ADDRESSES=(10.138.1.11
                  10.138.0.12
                  10.10.1.242)
    if [ "$1" == "-s" ] || [ "$1" == "--sequential" ]; then
      echo "Executing checkvpn sequentially..."
      echo
      _checkvpn
    elif [ -z "$1" ]; then
      echo "Checking VPN ..."
      for ip in "${IP_ADDRESSES[@]}" ; do
           ( _vpn_ping $ip )&
       done
       wait
       echo
    else
      echo "USAGE:"
      echo "  checkvpn [-s | --sequential]"
      echo
    fi
}

function buildtesthelper {
    if [[ -z "${TEST_HELPER_FX_SRC}" ]]; then
        TEST_HELPER_FX_SRC=~/src/TestHelperFx
    else
        echo "Using source folder: ${TEST_HELPER_FX_SRC}"
        echo
    fi

    if [ ! -d "${TEST_HELPER_FX_SRC}" ]; then
        echo "TestHelperFx source folder not found"
        echo "You can clone it to your src folder with:"
        echo "cd ~/src && git clone git@gitlab.com:vetsource/seti-services/misc/TestHelperFx.git"
        echo
        echo "Retry after doing that."
        echo "  If using a different source folder,"
        echo "  you can set TEST_HELPER_FX_SRC env variable with the custom path"
        echo
        return 1
    fi

    cd ${TEST_HELPER_FX_SRC}

    # prevent gradle timeout trying to access http://build.vetsource.com/artifactory/libs-releases/org/gebish/geb-spock/maven-metadata.xml
    TIMEOUT="60"  # seconds
    GRADLE_OPTS="-Dorg.gradle.internal.http.socketTimeout=${TIMEOUT}000 -Dorg.gradle.internal.http.connectionTimeout=${TIMEOUT}000"
    
    # requires gradle 4.8.1, which requires sdkman, so import the sdk() function just in case
    export SDKMAN_DIR="/home/developer/.sdkman"
    [[ -s "/home/developer/.sdkman/bin/sdkman-init.sh" ]] && source "/home/developer/.sdkman/bin/sdkman-init.sh"
    sdk use gradle 4.8.1
    if [[ "$?" -ne "0" ]]; then
        # gradle 4.8.1 is not installed, aborting
        echo
        return 1
    fi

    gradle build $GRADLE_OPTS
    cd -
}

function listalias {
    echo
    echo "List all available alias"
    echo "======================="
    echo
    echo "clearmemcache"
    echo "mysqlflush"
    echo
    echo "devstack_start"
    echo "devstack_status"
    echo "devstack_stop"
    echo
    echo "checkvpn"
    echo "startvpn"
    echo "stopvpn"
    echo
    echo "buildtesthelper"
    echo
}
